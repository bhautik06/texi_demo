const express = require('express')
const app = express()
const chalk = require('chalk')
const morgan = require('morgan')
const port = 3000
const driverRoute = require('./routes/driver')
const custRoute = require('./routes/customer')
const booingRoute = require('./routes/booking')

app.use(morgan('short'))
app.use(express.json())
app.use(express.urlencoded({extended:false}))

app.use('/api', driverRoute)
app.use('/api', custRoute)
app.use('/api', booingRoute)

app.listen(port, () => {
    console.log(`${chalk.green('✔')} server running on port ${chalk.blueBright(port)}`)
})