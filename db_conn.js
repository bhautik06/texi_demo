const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017/taxi_db')
    .then(() => console.log('db connected...!'))
    .catch(err => console.log('could not connect with DB'))

module.exports = mongoose