const jwt = require('jsonwebtoken')

function checkDriverToken(req, res, next) {
    jwt.verify(req.token, 'driverSecretkey', (err, authData) => {
        if (err) {
            return res.status(400).json({
                message: 'Token Expire...!!'
            });
        }
        req.driver = authData;
        next();
    });
}

function checkCustomerToken(req, res, next) {
    jwt.verify(req.token, 'customerSecretkey', (err, authData) => {
        if (err) {
            return res.status(400).json({
                message: 'Token Expire...!!'
            });
        }
        req.customer = authData;
        next();
    });
}

function verifyToken(req, res, next) {
    const bearerHeader = req.headers['authorization']
    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    } else {
        res.status(400).json({
            message: 'Token Expire...!!'
        });
    }
}

module.exports = { jwt, checkDriverToken, checkCustomerToken, verifyToken }