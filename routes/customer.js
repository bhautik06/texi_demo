const express = require('express')
const router = express.Router()
const { jwt, verifyToken, checkCustomerToken } = require('./jwtTokens')
const Customer = require('../models/customers')

router.get('/customer', (req, res) => {
    Customer
        .find()
        .then((dr) => {
            console.log(dr);
            res.send(dr)
        })
});

router.post('/customer/signup', (req, res) => {
    Customer
        .findOne({
            email: req.body.email
        })
        .then((c) => {
            if (!c) {
                const cust = new Customer({
                    name: req.body.name,
                    email: req.body.email,
                    city: req.body.city,
                    state: req.body.state,
                    country: req.body.country,
                    phoneNumber: req.body.phoneNumber,
                    password: req.body.password
                });
                cust.save();
                console.log(cust);
                res.send({
                    message: 'Your Registration Successful',
                    cust
                })
            } else {
                res.send({
                    message: 'Customer Already Exist'
                })
            }
        })
});

router.post('/customer/signin', (req, res) => {
    Customer
        .findOne({
            email: req.body.email
        })
        .then((d) => {
            if (req.body.password == d.password) {
                jwt.sign({ id: d.id }, 'customerSecretkey', { expiresIn: '2h' }, (err, token) => {
                    res.send({
                        message: 'Login Successful!',
                        token: token,
                    })
                });
            } else[
                res.send({
                    message: 'Password Incorrect!'
                })
            ]
        })
})

router.put('/customer/update', verifyToken, checkCustomerToken, (req, res) => {
    Customer
        .findByIdAndUpdate(`${req.customer.id}`, {
            $set: {
                name: req.body.name,
                email: req.body.email,
                city: req.body.city,
                state: req.body.state,
                country: req.body.country,
                phoneNumber: req.body.phoneNumber,
                password: req.body.password
            }
        },
            { new: true })
        .then(() => {
            res.send({
                message: 'Details Updated'
            })
        })
        .catch(err => res.send({ error: err.message }))
});

router.delete('/customer/remove', verifyToken, checkCustomerToken, (req, res) => {
    Customer
        .findByIdAndRemove(`${req.customer.id}`)
        .then((dr) => {
            if (!dr) {
                return res.status(404).send({
                    message: 'The given Id not found'
                });
            }
            res.send({
                message: 'Deleted'
            })
        })
});

module.exports = router