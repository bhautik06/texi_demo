const express = require('express')
const router = express.Router()
const { jwt, verifyToken, checkDriverToken } = require('./jwtTokens')
const Driver = require('../models/drivers')

router.get('/driver', (req, res) => {
    Driver
        .find()
        .then((dr) => {
            console.log(dr);
            res.send(dr)
        })
});

router.post('/driver/signup', (req, res) => {
    Driver
        .findOne({
            email: req.body.email
        })
        .then((d) => {
            if (!d) {
                const driver = new Driver({
                    name: req.body.name,
                    email: req.body.email,
                    city: req.body.city,
                    state: req.body.state,
                    country: req.body.country,
                    phoneNumber: req.body.phoneNumber,
                    password: req.body.password,
                    vehicle: {
                        vehicle_name: req.body.veh_name,
                        vehicle_model: req.body.veh_model,
                        vehicle_number: req.body.veh_number,
                        color: req.body.color
                    },
                    location: [req.body.latitude, req.body.longitude]
                });
                driver.save();
                console.log(driver);
                res.send({
                    message: 'Your Registration Successful',
                    driver
                })
            } else {
                res.send({
                    message: 'Driver Already Exist'
                })
            }
        })
});

router.post('/driver/signin', (req, res) => {
    Driver
        .findOne({
            email: req.body.email
        })
        .then((d) => {
            if (req.body.password == d.password) {
                jwt.sign({ id: d.id }, 'driverSecretkey', { expiresIn: '2h' }, (err, token) => {
                    res.send({
                        message: 'Login Successful!',
                        token: token,
                    })
                });
            } else[
                res.send({
                    message: 'Password Incorrect!'
                })
            ]
        })
        .catch(() => {
            res.send({
                message: 'Email not found'
            })
        })
})

router.put('/driver/update', verifyToken, checkDriverToken, (req, res) => {
    Driver
        .findByIdAndUpdate(`${req.driver.id}`, {
            $set: {
                name: req.body.name,
                email: req.body.email,
                city: req.body.city,
                state: req.body.state,
                country: req.body.country,
                phoneNumber: req.body.phoneNumber,
                password: req.body.password,
                vehicle: {
                    vehicle_name: req.body.veh_name,
                    vehicle_model: req.body.veh_model,
                    vehicle_number: req.body.veh_number,
                    color: req.body.color
                },
                location: {
                    latitude: req.body.latitude,
                    longitude: req.body.longitude
                }
            }
        },
            { new: true })
        .then(() => {
            res.send({
                message: 'Details Updated'
            })
        })
        .catch(err => res.send({ error: err.message }))
});

router.delete('/driver/remove', verifyToken, checkDriverToken, (req, res) => {
    Driver
        .findByIdAndRemove(`${req.driver.id}`)
        .then((dr) => {
            if (!dr) {
                return res.status(404).send({
                    message: 'The given Id not found'
                });
            }
            res.send({
                message: 'Deleted'
            })
        })
});

module.exports = router