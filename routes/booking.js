const express = require('express')
const router = express.Router()
const { verifyToken, checkCustomerToken } = require('./jwtTokens')
const Customer = require('../models/customers')
const Driver = require('../models/drivers')
const Booking = require('../models/bookings')

router.get('/cab/allCab', (req, res) => {
    Driver
        .find()
        .then((cab) => {
            res.send({
                count: cab.length,
                allCab: cab
            })
        })
})

router.post('/cab/booking', verifyToken, checkCustomerToken, async (req, res) => {
    const { latitude, longitude } = req.body
    Driver
        .find({
            location: {
                $near: [latitude, longitude],
                $maxDistance: 1
            }
        }, function (err, cabs) {
            if (err) return console.log(err);
            cabs.forEach(function (cab) {
                if (!cab.isBooked) {
                    Driver
                        .findByIdAndUpdate(cab.id, {
                            isBooked: true
                        })
                        .then((cabBook) => {
                            Customer
                                .findById(`${req.customer.id}`)
                                .then((cust) => {
                                    const booking = new Booking({
                                        driverName: cabBook.name,
                                        driverNumber: cabBook.phoneNumber,
                                        vehicleNumber: cabBook.vehicle.vehicle_number,
                                        customerName: cust.name,
                                        customerNumber: cust.phoneNumber,
                                        location: [latitude, longitude]
                                    })
                                    booking.save()
                                    res.send({
                                        cabs: 'Cab Booked..!',
                                        details: booking
                                    })
                                })
                        })
                } else {
                    res.send({
                        msg: 'Cab not available'
                    })
                }
            })
        });
})

module.exports = router