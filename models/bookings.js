const mongoose = require('../db_conn')

module.exports = mongoose.model('Booking', new mongoose.Schema({
	driverName: {
		type: String,
		default: ''
	},
	driverNumber: {
		type: Number,
		default: ''
	},
	vehicleNumber: {
		type: String,
		default: ''
	},
	customerName: {
		type: String,
		default: ''
	},
	customerNumber: {
		type: String,
		default: ''
	},
	location:{
        type : [Number],
        index: '2d'
    },
	date: {
		type: Date,
		default: Date.now()
	}
}));
