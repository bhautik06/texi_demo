const mongoose = require('../db_conn')

module.exports = mongoose.model('Customer', new mongoose.Schema({
    name: {
		type: String,
		default: ''
	},
	email: {
		type: String,
		default: ''
	},
	city: {
		type: String,
		default: ''
	},
	state: {
		type: String,
		default: ''
	},
	country: {
		type: String,
		default: ''
	},
	phoneNumber: {
		type: String,
		default: ''
	},
	password: {
		type: String,
		default: ''
	},
    date: {
        type: Date,
        default: Date.now()
	}
}));
