const mongoose = require('../db_conn')

module.exports = mongoose.model('Driver', new mongoose.Schema({
    name: {
		type: String,
		default: ''
	},
	email: {
		type: String,
		default: ''
	},
	city: {
		type: String,
		default: ''
	},
	state: {
		type: String,
		default: ''
	},
	country: {
		type: String,
		default: ''
	},
	phoneNumber: {
		type: String,
		default: ''
	},
	password: {
		type: String,
		default: ''
	},
    date: {
        type: Date,
        default: Date.now()
	},
	isBooked: {
		type: Boolean,
        default: false
	},
	vehicle: {
		vehicle_name: {
			type: String,
			default: ''
		},
		vehicle_model: {
			type: String,
			default: ''
		},
		vehicle_number: {
			type: String,
			default: ''
		},
		color: {
			type: String,
			default: ''
		}
	},
	location:{
        type : [Number],
        index: '2d'
    },
}));
